#ifndef GAMESAVEWRITER_H
#define GAMESAVEWRITER_H
#include <gamesave.h>
#include <QString>

/**
 * @brief Klasa odpowiedzialna za zapisywanie stanu gry do pliku.
 */
class GameSaveWriter
{
public:
    /**
     * @brief Konstruktor.
     */
    GameSaveWriter();
    /**
     * @brief Metoda zapisująca stan gry do pliku.
     * @param gameSave Stan gry do serializacji.
     * @param filepath Ścieżka do pliku.
     */
    void writeGameSaveTo(GameSave* gameSave, QString filepath);
};

#endif // GAMESAVEWRITER_H
