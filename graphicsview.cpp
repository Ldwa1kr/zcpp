#include "graphicsview.h"




GraphicsView::GraphicsView(QGraphicsScene* scene): QGraphicsView(scene)
{

}

void GraphicsView::mousePressEvent(QMouseEvent *event){
    QGraphicsView::mousePressEvent(event);
}

void GraphicsView::wheelEvent(QWheelEvent *event)
{
       setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
       double scaleFactor = 1.15;

       if(event->delta() > 2){
            scale(scaleFactor, scaleFactor);
       } else {
           scale(1/scaleFactor, 1/scaleFactor);
       }
}
