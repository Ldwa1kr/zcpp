#ifndef GRAPHICSSCENE_H
#define GRAPHICSSCENE_H
#include <QGraphicsView>
#include <QWidget>
#include <QWheelEvent>

/**
 * @brief Klasa przedstawiająca widok gry.
 */
class GraphicsView : public QGraphicsView
{
public:
    /**
     * @brief Kostruktor.
     * @param Scena gry.
     */
    GraphicsView(QGraphicsScene* scene);
protected:
    /**
     * @brief Handler kliknięcia na widok gry.
     * @param event Wydarzenie kliknięcia.
     */
    void mousePressEvent(QMouseEvent *event);
    /**
     * @brief Handler oddalania/przybliżania przy pomocy kółka myszki.
     * @param event Wydarzenie przybliżania/oddalania.
     */
    virtual void wheelEvent(QWheelEvent * event);
};

#endif // GRAPHICSSCENE_H
