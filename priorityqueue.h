#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H
#include <queue>


/**
 * @param Value Wartość przechowywana przez kolejkę.
 * @param Priority Priorytet elementu.
 * @param Capacity pojemność kolejki.
 */
template<typename Value, typename Priority, int Capacity>
/**
 * @brief Kolejka priorytetowa.
 */
struct PriorityQueue {
  typedef std::pair<Value, Priority> Element;
   /**
   * @brief Komparator elementów ustawiający elementy od największych.
   */
  struct greater : public std::binary_function<Element, Element, bool>
  {
    _GLIBCXX14_CONSTEXPR
    bool
    operator()(const Element& one, const Element& two) const
    { return one.second > two.second; }
  };

  std::priority_queue<Element, std::vector<Element>,
                 PriorityQueue::greater> elements;
  /**
   * @brief Metoda zwracająca informację czy kolejka jest pusta.
   * @return Czy kolejka jest pusta.
   */
  inline bool isEmpty() const {
     return elements.empty();
  }

  /**
   * @brief Metoda wkładająca element do kolejki.
   * @param value Wartość.
   * @param priority Priorytet.
   */
  inline void push(Value value, Priority priority) {
      if(elements.size() >= Capacity){
          return;
      }
    elements.emplace(value, priority);
  }

  /**
   * @brief Metoda zwracająca wielkość kolejki.
   * @return Wielkość kolejki.
   */
  inline int size() {
      return elements.size();
  }

  /**
   * @brief Metoda zwracająca największy element z kolejki.
   * @return Największy element.
   *  Metoda zwracająca największy element z kolejki.
   * Najlepszy element jest również wyjęty z kolejki.
   */
  Value pop() {
    Value returnValue = elements.top().first;
    elements.pop();
    return returnValue ;
  }
};
#endif // PRIORITYQUEUE_H
