#include "piece.h"

Piece::Piece(int col, int row, int value):
    col(col),
    row(row),
    value(value)
{}

QRectF Piece::boundingRect() const
{
    return QRectF(col * WIDTH, row * WIDTH, WIDTH, WIDTH);
}

int Piece::getValue()
{
    return value;
}

void Piece::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    auto rect = QRectF(col * WIDTH, row * WIDTH, WIDTH, WIDTH);
    if(value){
        painter->drawText(rect,Qt::AlignCenter, QString::number(value));
    }
    painter->drawRect(rect);
}

int Piece::getColumn(){
    return col;
}

int Piece::getRow(){
    return row;
}

void Piece::setColumnAndRow(int col, int row){
    this->col = col;
    this->row = row;
}

void Piece::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    emit clicked(col, row);
}

