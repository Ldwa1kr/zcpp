#include "gamesave.h"


GameSave::GameSave()
{
    this->gameState = new std::vector<int>();
    this->moves =0;
}

GameSave::GameSave(int gameSize) {
    this->gameSize = gameSize;
    this->gameState = new std::vector<int>();
    for(int i = 0; i < gameSize * gameSize; i++){
        gameState->push_back(i);
    }
    this->moves =0;
}

GameSave::GameSave(std::vector<int> *gameState, int gameSize, int moves){
    this->gameState = gameState;
    this->gameSize = gameSize;
    this->moves =moves;
}

GameSave::~GameSave(){
    delete gameState;
}

void GameSave::incrementMove(){
    this->moves++;
}

std::vector<int>& GameSave::getGameState()
{
    return *gameState;
}

int GameSave::getGameSize(){
    return gameSize;
}

int GameSave::getMoves()
{
    return moves;
}

std::vector<int>::iterator GameSave::gameStateStart() {
    return gameState->begin();
}

std::vector<int>::iterator GameSave::gameStateEnd() {
    return gameState->end();
}


