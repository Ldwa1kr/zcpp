#include "gamewindow.h"
#include <QPushButton>
#include <QGridLayout>
#include <memory>
#include <QGraphicsView>
#include <QGraphicsScene>
#include "piece.h"
#include "graphicsview.h"
#include <QFileDialog>
#include "keypresslistener.h"
#include <QLabel>
#include <QHBoxLayout>

GameWindow::GameWindow(QWidget *parent) : QWidget(parent)
{
    this->setMinimumSize(500, 500);
    scene = new QGraphicsScene;

    QPushButton *startMenuButton = new QPushButton("Opcje");
    this->view = new GraphicsView(scene);

    this->gameEngine = new GameEngine;
    this->gameEngine->setScene(scene);

    auto eventFilter = new KeyPressListener(this);
    connect(eventFilter,
            SIGNAL(moveRequested(Transformation)),
            gameEngine,
            SLOT(userMovePuzzle(Transformation)));
    this->installEventFilter(eventFilter);

    moveCounter = new QLabel();
    connect(gameEngine, SIGNAL(moveCountChanged()), this, SLOT(updateMoveCounter()));

    QGridLayout *layout = new QGridLayout;
    QHBoxLayout * buttonLayout = new QHBoxLayout;

    QPushButton *hintButton = new QPushButton("Podpowiedź");
    connect(hintButton, SIGNAL(clicked(bool)), this, SLOT(hint(bool)));

    QPushButton *solveButton = new QPushButton("Rozwiaz");
    connect(solveButton, SIGNAL(clicked(bool)), this, SLOT(solve(bool)));

    heuristicSelection = new QComboBox(this);
    heuristicSelection->addItem(gameEngine->MANHATTAN_DISTANCE_HEURISTIC);
    heuristicSelection->addItem(gameEngine->DISPLACED_HEURISTIC);


    buttonLayout->addWidget(startMenuButton);
    buttonLayout->addWidget(heuristicSelection);
    buttonLayout->addWidget(hintButton);
    buttonLayout->addWidget(solveButton);
    buttonLayout->addWidget(moveCounter);
    layout->addWidget(view, 0, 0);
    layout->addLayout(buttonLayout, 1, 0);
    this->setLayout(layout);
    connect(startMenuButton, SIGNAL(clicked(bool)), this, SLOT(openStartMenu(bool)));
}

GameWindow::~GameWindow(){
    delete gameEngine;
    delete gameSave;
}

void GameWindow::setStartMenu(StartMenu* startmenu) {
    this->startMenu = startmenu;
}

void GameWindow::openStartMenu(bool clicked){
    startMenu->show();
}

void GameWindow::solve(bool)
{
    gameEngine->solve(this->heuristicSelection->currentText());
}

void GameWindow::hint(bool)
{
    gameEngine->hint(this->heuristicSelection->currentText());
}

void GameWindow::openGame(GameSave *gameSave, bool randomize){
    if(this->gameSave != nullptr){
        delete this->gameSave;
    }
    this->gameSave = gameSave;
    this->gameEngine->setGameSave(gameSave);
    if(randomize){
        auto gameSize = gameSave->getGameSize();
        if( gameSize < 5 ){
            this->gameEngine->randomize(120);
        } else {
            this->gameEngine->randomize(gameSize * gameSize * 10);
        }

    }
}

void GameWindow::updateMoveCounter()
{
    auto currentMoves = QString::number(this->gameEngine->getMoveCount());
    this->moveCounter->setText("Licznik ruchów: " + currentMoves);
}

void GameWindow::saveGame(bool clicked)
{
    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Zapisz grę"),
                                                    "/home/",
                                                    tr("1337 files(*.leet)"));
    if(filename.size()){
        gameEngine->saveGame(filename);
    }
}

