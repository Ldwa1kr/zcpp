#include "startmenu.h"
#include <QApplication>
#include "gamewindow.h"
#include <memory>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    GameWindow gameWindow;
    StartMenu menu(&gameWindow);
    gameWindow.setStartMenu(&menu);
    menu.show();

    return a.exec();
}
