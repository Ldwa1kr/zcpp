#include "puzzlesolver.h"
#include <unordered_map>
#include <transformation.h>
#include <priorityqueue.h>
#include <algorithm>

PuzzleSolver::PuzzleSolver()
{

}


int PuzzleSolver::manhattanDistance(SolutionNode node)
{
    int cost =0;
    int limit = node.getGameSize() * node.getGameSize();
    int gameSize = node.getGameSize();
    for(int currentIndex = 0; currentIndex< limit; currentIndex++){
        int currentCol = currentIndex % gameSize;
        int currentRow = currentIndex / gameSize;
        int targetIndex = node.getState()[currentIndex];
        int targetCol = targetIndex % gameSize;
        int targetRow = targetIndex / gameSize;

        cost += std::abs(targetCol - currentCol) + std::abs(targetRow - currentRow);
    }

    return cost;
}

int PuzzleSolver::displacedPieces(SolutionNode node)
{
    int displaced =0;
    int limit = node.getGameSize() * node.getGameSize();
    for(int i= 0; i< limit; i++){
        if(node.getState()[i] != i) {
            displaced++;
        }
    }
    return displaced;
}


std::vector<Transformation> PuzzleSolver::solve(GameSave& gameSave,
                         std::function<int (SolutionNode)> heuristic) {
    auto gameSize = gameSave.getGameSize();
    SolutionNode end(GameSave(gameSize).getGameState(), gameSize, Transformation::NONE);
    SolutionNode start(gameSave.getGameState(), gameSize, Transformation::NONE);
    std::unordered_map<SolutionNode, SolutionNode> parents;
    std::unordered_map<unsigned long, int> costToReach;

    PriorityQueue<SolutionNode, int, 200000> frontier;
    frontier.push(start, 0);

    costToReach[start.getHash()] = 0;
    parents[start] = start;
    SolutionNode solution;
    while(!frontier.isEmpty()){
        SolutionNode current = frontier.pop();
        if(current.getHash() == end.getHash()){
            solution = current;
            break;
        }
        auto newStates  =current.getNextStates();
        for (auto const &next : newStates) {
            auto newCost = costToReach[current.getHash()] + 1 ;
              if (costToReach.find(next.getHash()) == costToReach.end()
                  || newCost < costToReach[next.getHash()]) {
                costToReach[next.getHash()] = newCost;
                auto priority = newCost+ heuristic(next);
                frontier.push(next, priority);
                parents[next] = current;
              }
            }
    }
    return getTransformationPath(solution, start, parents);
}

std::vector<Transformation> PuzzleSolver::getTransformationPath(SolutionNode goal,
                                                                SolutionNode start,
                                                                std::unordered_map<SolutionNode, SolutionNode> parents) {
    std::vector<Transformation> transformations;
    SolutionNode current = goal;
    while (goal.getHash() != start.getHash()) {
        if(current.getHash() == start.getHash()) {
            break;
        }
      transformations.push_back(current.getTransformation());
      current = parents[current];
    }
    std::reverse(transformations.begin(), transformations.end());
    return transformations;
}
