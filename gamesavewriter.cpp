#include "gamesavewriter.h"
#include "gamesave.h"
#include <QString>
#include <QTextStream>
#include <QFile>
#include <QIODevice>

GameSaveWriter::GameSaveWriter()
{

}

void GameSaveWriter::writeGameSaveTo(GameSave* gameSave, QString filepath) {
    QFile file(filepath);
    file.open(QIODevice::WriteOnly);
    QTextStream text(&file);
    text << gameSave->getGameSize() << "\n";
    text << gameSave->getMoves() << "\n";
    for(auto iter = gameSave->gameStateStart();
        iter != gameSave->gameStateEnd();
        iter++){
        text << *iter << "\n";
    }

    file.close();
}
