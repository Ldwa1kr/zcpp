#ifndef GAMESAVE_H
#define GAMESAVE_H
#include <vector>

/**
 * @brief Klasa przetrzymująca zapis stanu gry.
 */
class GameSave
{
public:
    /**
     * @brief Konstruktor.
     * @param gameState Wektor numerów elementów układanki.
     * @param gameSize Rozmiar gry.
     * @param moves Ilosć ruchów od rozpoczęcia gry.
     */
    GameSave(std::vector<int> *gameState, int gameSize, int moves);
    /**
     * @brief Konstruktor.
     * @param gameSize Rozmiar gry.
     */
    GameSave(int gameSize);
    /**
     * @brief Konstruktor
     */
    GameSave();
    /**
     * @brief Destruktor
     */
    ~GameSave();
    /**
     * @brief Metoda zwracająca rozmiar gry.
     * @return Rozmiar gry.
     */
    int getGameSize();
    /**
     * @brief Metoda zwracająca ilość ruchów od rozpoczęcia gry.
     * @return Ilość ruchów.
     */
    int getMoves();
    /**
    * @brief Metoda zwiększająca licznik ruchów.
     */
    void incrementMove();
    /**
     * @brief Metoda zwracająca stan gry.
     * @return Stan gry.
     */
    std::vector<int>& getGameState();
    /**
     * @brief Metoda zwracająca iterator początka stanu gry.
     * @return Iterator na początek stanu gry.
     */
    std::vector<int>::iterator gameStateStart();
    /**
     * @brief Metoda zwracająca iterator na koniec stany gry.
     * @return Iterator na koniec stanu gry.
     */
    std::vector<int>::iterator gameStateEnd();
private:
    /**
     * @brief Rozmiar gry. Dla układanki 3x3 wynosi 3.
     */
    int gameSize;
    /**
     * @brief Licznik ruchów od początku gry.
     */
    int moves;
    /**
     * @brief Stan gry.
     */
    std::vector<int>* gameState;
};

#endif // GAMESAVE_H
