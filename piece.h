#ifndef PIECE_H
#define PIECE_H
#include <QGraphicsItem>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QWidget>
#include <QRectF>
#include <QObject>

/**
 * @brief Element układanki.
 */
class Piece : public QGraphicsObject
{
Q_OBJECT
public:
    /**
     * @brief Konstruktor.
     * @param col Kolumna w ukłądance.
     * @param row Rząd w układance.
     * @param value Wartość elementu.
     */
    Piece(int col, int row, int value);
    /**
     * @brief Metoda zwracająca obręb graniczny elementu.
     * @return Obręb graniczny.
     */
    QRectF boundingRect() const override;
    /**
     * @brief Metoda zwracająca wartość elementu.
     * @return Wartość elementu.
     */
    int getValue();
    /**
     * @brief Metoda ustawiająca kolumnę i rząd elementu.
     * @param col Kolumna elementu.
     * @param row Rząd elementu.
     */
    void setColumnAndRow(int col, int row);
    /**
     * @brief Metoda zwracająca kolumnę elementu.
     * @return Kolumna elementu.
     */
    int getColumn();
    /**
     * @brief Metoda zwracająca rząd elementu.
     * @return Rząd elementu.
     */
    int getRow();
    /**
     * @brief Metoda malująca element.
     * @param painter
     * @param option
     * @param widget
     */
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                   QWidget *widget) override;
    /**
     * @brief Metoda obsługująca wydarzenie kliknięcia.
     * @param event Wydarzenie.
     */
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override ;
signals:
    /**
     * @brief Sygnał kliknięcia w element.
     * @param col Kolumna elementu.
     * @param row Rząd elementu.
     */
    void clicked(int col, int row);
private:
    /**
     * @brief Szerokość elementu.
     */
    static constexpr int WIDTH = 50;
    /**
     * @brief Rząd elementu.
     */
    int row =0;
    /**
     * @brief Kolumna elementu.
     */
    int col =0;
    /**
     * @brief Wartość elementu.
     */
    int value = 0;
};

#endif // PIECE_H
