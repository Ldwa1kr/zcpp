#ifndef PUZZLESOLVER_H
#define PUZZLESOLVER_H
#include <solutionnode.h>
#include <queue>
#include <functional>
#include <transformation.h>
#include <vector>
#include <unordered_map>

/**
 * @brief Klasa zawierająca logikę.
 */
class PuzzleSolver
{
public:
    /**
     * @brief Konstruktor.
     */
    PuzzleSolver();
    /**
     * @brief Funkcja heurystyczna obliczająca koszt metodą odległość Manhattańskiej.
     * @param node SolutionNode do obliczenia odległości.
     * @return odległość.
     */
    static int manhattanDistance(SolutionNode node);
    /**
     * @brief Funkcja heurystyczna obliczjąca koszt metodą nieuporządkowanych elementów.
     * @param node SolutionNode do obliczenia odległości.
     * @return odległość.
     */
    static int displacedPieces(SolutionNode node);
    /**
     * @brief Funkcja rozwiązująca układankę.
     * @param gameSave stan gry.
     * @param heuristic Funkcja heurystyczna używana do obliczenia kosztu .
     * @return Wektor transformacji jakie potrzeba wykonać by rozwiązać układankę.
     */
    std::vector<Transformation> solve(GameSave& gameSave,
               std::function<int (SolutionNode)> heuristic );
private:
    SolutionNode transform(SolutionNode node, Transformation);
    /**
     * @brief Funkcja odbudowująca kolejność transformacji jakie trzeba wykonać w celu rozwiązania
     * układanki.
     * @param goal SolutionNode które jest stanem końcowym.
     * @param start SolutionNode które jest stanem początkowym.
     * @param parents vektor zawierający rodziców określonych SolutionNode.
     * @return Wektor transformacji jakie potrzeba wykonać by rozwiązać układankę.
     */
    std::vector<Transformation> getTransformationPath(SolutionNode goal,
                                                      SolutionNode start,
                                                      std::unordered_map<SolutionNode, SolutionNode> parents);
    /**
     * @brief Wielkość gry, dla wygody odnoszenia się.
     */
    int gameSize;
};

#endif // PUZZLESOLVER_H
