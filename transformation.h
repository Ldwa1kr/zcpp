#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

/**
 * @brief Enum transformacji jakie można wykonać na planszy gry.
 * Poszczególne wartości odpowiadają kierunkom w których można przesunąć
 * elementy układanki.
 */
enum Transformation {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    NONE
};

#endif // TRANSFORMATION_H
