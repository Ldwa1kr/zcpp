#ifndef PARSINGEXCEPTION_H
#define PARSINGEXCEPTION_H
#include <stdexcept>

/**
 * @brief Wyjątek rzucany podczas wystąpienia błędów przy deserializacji Gry.
 */
class ParsingException : public std::exception {
public:
    ParsingException(std::string const message): message(message) {
    }
    virtual char const *what() const noexcept {
        return message.c_str();
    }
private:
    std::string message;
};
#endif // PARSINGEXCEPTION_H
