#ifndef KEYPRESSLISTENER_H
#define KEYPRESSLISTENER_H

#include <QObject>
#include "transformation.h"

/**
 * @brief klasa zawierająca logikę przechwytującą kliknięcia klawiszy hjkl
 * w celu poruszenia klockami.
 */
class KeyPressListener : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Konstruktor.
     * @param parent Rodzic.
     */
    explicit KeyPressListener(QObject *parent = nullptr);
protected:
    /**
     * @brief Metoda filtrująca wydarzenia kliknięcia klawiatury.
     * @param obj
     * @param event
     */
    bool eventFilter(QObject* obj, QEvent* event);

signals:
    /**
     * @brief Sygnał do poruszenia elementem na planszy transformacją.
     */
    void moveRequested(Transformation);
public slots:
};

#endif // KEYPRESSLISTENER_H
