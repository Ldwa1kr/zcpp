#include "gamesaveparser.h"
#include <iostream>
#include <fstream>
#include <QFile>
#include <QTextStream>
#include "parsingexception.h"

GameSaveParser::GameSaveParser()
{

}

/**
 * @brief Parses a savegame file and constructs a GameSave
 * @param filepath filepath to the save file
 * @return pointer to constructed GameSave
 */
GameSave* GameSaveParser::parse(QString filepath) {
    QFile file(filepath);
    file.open(QIODevice::ReadOnly);
    QTextStream text(&file);

    int gameSize = 0;
    text >> gameSize;
    if(!gameSizeValid(gameSize)){
        file.close();
        throw ParsingException("Invalid puzzle size");
    }
    int moves =0;
    text >> moves;

    auto * gameState = new std::vector<int>;
    while(!text.atEnd()){
        int gamePiece =-1;
        text >> gamePiece;
        gameState->push_back(gamePiece);
    }
    gameState->pop_back();

    GameSave* gameSave = new GameSave(gameState, gameSize, moves);

    file.close();
    return gameSave;
    //cleanup
    //delete gameState;
}

bool GameSaveParser::gameSizeValid(int gameSize){
    return (gameSize >= 2 && gameSize < 46000);
}
