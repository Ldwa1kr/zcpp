#include "keypresslistener.h"
#include "transformation.h"
#include <QEvent>
#include <QKeyEvent>

KeyPressListener::KeyPressListener(QObject *parent) : QObject(parent)
{

}

bool KeyPressListener::eventFilter(QObject* obj, QEvent* event)
{
    if (event->type()==QEvent::KeyPress) {
        QKeyEvent* key = static_cast<QKeyEvent*>(event);
        if ( key->key()==Qt::Key_L ) {
            emit moveRequested(Transformation::RIGHT);
        } else if( key->key()==Qt::Key_H) {
            emit moveRequested(Transformation::LEFT);
        } else if( key->key()==Qt::Key_K){
            emit moveRequested(Transformation::UP);
        } else if( key->key()==Qt::Key_J) {
            emit moveRequested(Transformation::DOWN);
        } else {
            return QObject::eventFilter(obj, event);
        }
        return true;
    } else {
        return QObject::eventFilter(obj, event);
    }
    return false;
}
