#include "solutionnode.h"
#include <vector>
#include "transformation.h"

std::vector<SolutionNode> SolutionNode::getNextStates()
{
    auto zeroCol = zeroIndex % gameSize;
    auto zeroRow = zeroIndex / gameSize;

    std::vector<SolutionNode> newNodes;
    for (int t=0; t<4; t++) {
        auto transformation = static_cast<Transformation>(t);
        if((zeroCol != gameSize-1 && transformation == Transformation::LEFT)
                || (zeroCol != 0 && transformation == Transformation::RIGHT)
                || (zeroRow != 0 && transformation == Transformation::DOWN)
                || (zeroRow != gameSize-1 && transformation == Transformation::UP)){
            newNodes.push_back(transform(transformation));
        }
    }
    return newNodes;
}

SolutionNode SolutionNode::transform(Transformation transformation)
{
    std::vector<int> newState(state);
    std::vector<int>::size_type index;
    switch(transformation){
    case UP:
        index = zeroIndex + gameSize;
        break;
    case DOWN:
        index =  zeroIndex - gameSize;
        break;
    case LEFT:
        index = zeroIndex + 1;
        break;
    case RIGHT:
        index = zeroIndex - 1;
        break;
    }
    newState[zeroIndex] = state[index];
    newState[index] = 0;

    return SolutionNode(newState, gameSize, transformation);
}
