#ifndef SOLUTIONNODE_H
#define SOLUTIONNODE_H
#include <vector>
#include <gamesave.h>
#include <transformation.h>
#include <boost/container_hash/hash.hpp>




/**
 * @brief Klasa reprezentująca stan gry w procesie poszukiwania rozwiązania gry.
 */
class SolutionNode
{
public:
    /**
     * @brief  Domyślny konstruktor.
     */
    SolutionNode(){}
    /**
     * @brief Konstuktor pozwalający na stworzenie SolutionNode w określonym stanie.
     * @param state Stan gry
     * @param gameSize Rozmiar gry
     * @param transformation Transformacja jaka zaszła by dostać się do obecnego SolutionNode
     */
    SolutionNode(std::vector<int> state, int gameSize, Transformation transformation):
        state(state),
        hash(boost::hash_value(state)),
        gameSize(gameSize),
        transformation(transformation){

        int counter = 0;
        for(auto const &value : state ){
            if(value == 0){
                zeroIndex = counter;
                break;
            }
            counter++;
        }

    }

    /**
     * @brief operator == polegający na równości Hash'y obu SolutionNode.
     * @param other Inny SolutionNode.
     */
    bool operator==(const SolutionNode& other) const {
           return hash == other.getHash();
    }
    /**
     * @brief Zwraca wektor stanów do których można dojść poprzez dany SolutionNode.
     * @return Wektor nowych SolutionNode.
     */
    std::vector<SolutionNode> getNextStates();
    /**
     * @brief Zwraca stan gry.
     * @return Stan gry.
     */
    std::vector<int>& getState() {return state;}
    /**
     * @brief Zwraca hash.
     * @return Hash.
     */
    inline size_t getHash() const {return hash;}
    /**
     * @brief Zwraca rozmiar gry.
     * @return Rozmiar gry.
     */
    int getGameSize() {return gameSize;}
    /**
     * @brief Zwraca Transformację.
     * @return Transformacja przez którą powstała ta SolutionNode.
     */
    Transformation getTransformation(){return transformation;}
private:
    /**
     * @brief Indeks pola state na którym znajduje się puste pole 0.
     */
    int zeroIndex;
    /**
     * @brief Rozmiar gry, w przypadku gry 3x3 jest to 3.
     */
    int gameSize;
    /**
     * @brief Hash SolutionNode idendyfikujący dany stan gry.
     */
    std::size_t hash;
    /**
     * @brief Vector pól gry, reprezentujący aktualny stan gry.
     */
    std::vector<int> state;
    /**
     * @brief Transformacja
     */
    Transformation transformation;
    /**
     * @brief Przeprowadza transformację na aktualnym SolutionNode.
     * @param transformacja.
     * @return SolutionNode wynikający z przeprowadzenia transformacji.
     */
    SolutionNode transform(Transformation transformation);
};


namespace std {
/**
 * @brief Konkretyzcja funkcji szablonu konstrukcji hasha SolutionNode.
 */
template <> struct hash<SolutionNode> {
  std::size_t operator()(const SolutionNode& node) const noexcept {
    return node.getHash();
  }
};
}

#endif // SOLUTIONNODE_H
