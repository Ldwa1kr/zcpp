#ifndef STARTMENU_H
#define STARTMENU_H

#include <QWidget>
#include "QLineEdit"
#include "gamewindow.h"
#include <QPushButton>
class GameWindow;

/**
 * @brief Menu startowe gry.
 */
class StartMenu : public QWidget
{
    Q_OBJECT
public:
    /**
     * @brief Konstruktor.
     * @param gameWindow Okno gry.
     * @param parent Rodzic.
     */
    explicit StartMenu(GameWindow* gameWindow, QWidget *parent = nullptr);
private:
    /**
     * @brief Domyślna wielkość gry.
     */
    static constexpr int DEFAULT_GAME_SIZE = 3;
    /**
     * @brief gameWindow Okno gry.
     */
    GameWindow* gameWindow;
    /**
     * @brief Funkcja konstrująca widget do wpisywania rozmiaru gry.
     * @return Wskaźnik na utworzony widget.
     */
    QLineEdit* constructGameSizeInput();
    /**
     * @brief Pole do wpisywania rozmiaru gry.
     */
    QLineEdit * gameSizeInput;
    /**
     * @brief Przycisk do zapisywania gry.
     */
    QPushButton* saveGameButton;
signals:

public slots:
    /**
     * @brief Zapisywanie gry.
     * @param clicked
     */
    void loadGame(bool clicked);
    /**
     * @brief Otwieranie okna gry.
     */
    void openGameWindow();
    /**
     * @brief Otwieranie okna gry.
     * @param clicked
     */
    void openGameWindow(bool clicked);
    /**
     * @brief Metoda sanityzująca tekst.
     * @param text
     */
    void sanitizeInput(QString text);
};

#endif // STARTMENU_H
