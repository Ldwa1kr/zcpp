#ifndef GAMESAVEPARSER_H
#define GAMESAVEPARSER_H
#include "gamesave.h"
#include <QString>

/**
 * @brief Klasa zawierająca logikę deserializacji ciągu znaków do instancji obiektu GameSave
 */
class GameSaveParser
{
public:
    /**
     * @brief Konstruktor
     */
    GameSaveParser();
    /**
     * @brief Metoda parsująca plik pod podaną ścieżką.
     * @param filepath Ścieżka do pliku z zapisem gry.
     * @return Zdeserializowana instancja GameSave.
     */
    GameSave* parse(QString filepath);
private:
    /**
     * @brief Metoda determinująca rozmiar gry jest w akceptowalnych granicach.
     * @param gameSize Rozmiar gry.
     * @return Czy rozmiar gry jest akceptowalny.
     */
    bool gameSizeValid(int gameSize);
};

#endif // GAMESAVEPARSER_H
