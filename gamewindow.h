#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QWidget>
#include "startmenu.h"
#include <boost/scoped_ptr.hpp>
#include <QGraphicsScene>
#include "graphicsview.h"
#include "gameengine.h"
#include <QLabel>
#include <QComboBox>

class StartMenu;

/**
 * @brief Klasa reprezencująca okno gry
 */
class GameWindow : public QWidget
{
    Q_OBJECT
public:
    /**
     * @brief Konstruktor.
     * @param parent Rodzic.
     */
    explicit GameWindow(QWidget *parent = nullptr);
    /**
     * @brief Destruktor.
     */
    ~GameWindow();
    /**
     * @brief Metoda ustawiająca menu startowe.
     * @param startmenu Menu startowe.
     */
    void setStartMenu(StartMenu* startmenu);
    /**
     * @brief Metoda rozpoczynająca grę bazującą na zadanym zapisie gry.
     * @param gameSave Zapis gry.
     * @param randomize Flaga czy stan gry powinien być randomizowany.
     */
    void openGame(GameSave* gameSave, bool randomize = false);
private:
    /**
     * @brief Wybór funcji do obliczania kosztu ustawienia elementu.
     */
    QComboBox *heuristicSelection;
    /**
     * @brief Silnik gry zawierający logikę przebiegu gry.
     */
    GameEngine* gameEngine;
    /**
     * @brief Widok gry.
     */
    GraphicsView* view;
    /**
     * @brief Menu startowe.
     */
    StartMenu* startMenu;
    /**
     * @brief Scena na której są umieszczane elementy gry.
     */
    QGraphicsScene* scene;
    /**
     * @brief Zapis gry.
     */
    GameSave* gameSave = nullptr;
    /**
     * @brief Licznik ruchów od początku gry.
     */
    QLabel* moveCounter;
signals:

public slots:
    /**
     * @brief Slot aktualizujący licznik ruchów.
     */
    void updateMoveCounter();
    /**
     * @brief Metoda zapisująca stan gry.
     * @param clicked
     */
    void saveGame(bool clicked);
    /**
     * @brief Metoda otwierająca menu startowe.
     * @param clicked
     */
    void openStartMenu(bool clicked);
   /**
    * @brief Metoda zapoczątkowująca automatyczne rozwiązywanie układanki.
    */
    void solve(bool);
    /**
     * @brief Metoda podpowiadająca 1 ruch w kierunku rozwiązania układanki.
     */
    void hint(bool);
};

#endif // GAMEWINDOW_H
