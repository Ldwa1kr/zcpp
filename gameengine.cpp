#include "gameengine.h"
#include <QFileDialog>
#include <gamesavewriter.h>
#include <puzzlesolver.h>
#include <solutionnode.h>
#include <QTimer>
#include <QTime>
#include <QEventLoop>
#include <QCoreApplication>

GameEngine::GameEngine(QObject *parent) : QObject(parent)
{
    this->pieces = new std::vector<Piece*>;
}

GameEngine::~GameEngine(){
    delete[] pieces;
    delete gameSave;
}

void GameEngine::randomize(int iterations)
{
    if(gameSave == nullptr){
        return;
    }
    std::srand(std::time(nullptr));

    for(int i = 0; i< iterations; i++){
        Transformation t  = static_cast<Transformation>(std::rand() % 4);
        movePuzzle(t);
    }
}

void GameEngine::pieceClicked(int col, int row) {
    if(currentlySolving){
        return;
    }
    auto gameSize = gameSave->getGameSize();
    auto clickedIndex = gameSize* row+ col;
    Piece* clicked = (*pieces)[clickedIndex];

    Transformation transformation = getTransformation(clicked);
    if(transformation == Transformation::NONE) {
        return;
    }
    userMovePuzzle(transformation);
}

Transformation GameEngine::getTransformation(Piece* piece){
    auto row = piece->getRow();
    auto col = piece->getColumn();
    auto zeroRow = zeroPiece->getRow();
    auto zeroCol = zeroPiece->getColumn();
    if(col  == zeroCol){
        if(row - 1 == zeroRow){
            return Transformation::UP;
        }
        if(row + 1 == zeroRow){
            return Transformation::DOWN;
        }
    } else if(row == zeroRow){
        if(col +1 == zeroCol){
            return Transformation::RIGHT;
        }
        if(col -1 == zeroCol){
            return Transformation::LEFT;
        }
    }
    return Transformation::NONE;
}



void GameEngine::userMovePuzzle(Transformation transformation) {
    if(movePuzzle(transformation)){
        this->gameSave->incrementMove();
        emit moveCountChanged();
        refreshScene();
    }
    if(gameWon()){
        handleWin();
    }
}

void GameEngine::handleWin() {
    gameInProgress = false;
    scene->clear();
    QGraphicsTextItem * winTextItem = new QGraphicsTextItem;
    std::string winText;
    winText.append("Gratulacje, rozwiązane w : ")
            .append(std::to_string(gameSave->getMoves()))
            .append(" ruchów !");
    winTextItem->setPlainText(QString::fromStdString(winText));
    scene->addItem(winTextItem);
}

bool GameEngine::movePuzzle(Transformation transformation){
    if(!gameInProgress){
        return false;
    }
    auto zeroCol = zeroPiece->getColumn();
    auto zeroRow = zeroPiece->getRow();
    auto gameSize = gameSave->getGameSize();
    auto zeroIndex = zeroRow * gameSize + zeroCol;

    if((zeroCol == gameSize-1 && transformation == Transformation::LEFT)
            || (zeroCol == 0 && transformation == Transformation::RIGHT)
            || (zeroRow == 0 && transformation == Transformation::DOWN)
            || (zeroRow == gameSize-1 && transformation == Transformation::UP)){
        return false;
    }

    Piece* piece = nullptr;
    switch(transformation){
    case UP:
        piece = (*pieces)[zeroIndex + gameSize];
        break;
    case DOWN:
        piece = (*pieces)[zeroIndex - gameSize];
        break;
    case LEFT:
        piece = (*pieces)[zeroIndex + 1];
        break;
    case RIGHT:
        piece = (*pieces)[zeroIndex - 1];
        break;
    }
    if(piece != nullptr){
        swapPieceLocations(zeroPiece, piece);
        return true;
    }
    return false;
}

void GameEngine::updateGameSave() {
    auto gameIter = gameSave->gameStateStart();
    auto pieceIter = pieces->begin();
    for(; pieceIter != pieces->end(); pieceIter++, gameIter++){
        *gameIter = (*pieceIter)->getValue();
    }
}

void GameEngine::hint(QString heuristic) {
    if(!gameInProgress) {
        return;
    }
    currentlySolving = true;
    updateGameSave();
    auto chosenHeuristic = heuristic == MANHATTAN_DISTANCE_HEURISTIC
            ? PuzzleSolver::manhattanDistance
            : PuzzleSolver::displacedPieces;
    auto transformations = PuzzleSolver().solve(*gameSave, chosenHeuristic);
    userMovePuzzle(transformations[0]);
    currentlySolving = false;
}

void GameEngine::solve(QString heuristic)
{
    if(!gameInProgress) {
        return;
    }
    currentlySolving = true;
    updateGameSave();
    auto chosenHeuristic = heuristic == MANHATTAN_DISTANCE_HEURISTIC
            ? PuzzleSolver::manhattanDistance
            : PuzzleSolver::displacedPieces;
    auto transformations = PuzzleSolver().solve(*gameSave, chosenHeuristic);

    for(const auto& transformation : transformations){
        QTime dieTime= QTime::currentTime().addMSecs(200);
            while (QTime::currentTime() < dieTime)
                QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
            userMovePuzzle(transformation);
    }
    currentlySolving = false;
}

void GameEngine::swapPieceLocations(Piece* pieceOne, Piece* pieceTwo){
    auto colOne = pieceOne->getColumn();
    auto rowOne = pieceOne->getRow();
    auto colTwo = pieceTwo->getColumn();
    auto rowTwo = pieceTwo->getRow();

    auto gameSize = gameSave->getGameSize();
    auto indexOne = gameSize * rowOne + colOne;
    auto indexTwo = gameSize * rowTwo + colTwo;

    (*pieces)[indexOne] = pieceTwo;
    (*pieces)[indexTwo] = pieceOne;

    pieceOne->setColumnAndRow(colTwo, rowTwo);
    pieceTwo->setColumnAndRow(colOne, rowOne);
}

bool GameEngine::gameWon() {
    for(const auto &piece : *pieces){
        auto col = piece->getColumn();
        auto row = piece->getRow();
        auto value = piece->getValue();
        auto gameSize = gameSave->getGameSize();

        if((row*gameSize + col) != value){
            return false;
        }
    }
    return true;
}

void GameEngine::setScene(QGraphicsScene* scene){
    this->scene = scene;
}

void GameEngine::setGameSave(GameSave* gameSave) {
    this->gameSave = gameSave;
    this->gameInProgress=true;
    emit moveCountChanged();

    this->scene->clear();
    this->pieces->clear();
    auto iterStart = gameSave->gameStateStart();
    auto iterEnd = gameSave->gameStateEnd();
    auto gameSize = gameSave->getGameSize();
    int positionCounter= 0;
    for(; iterEnd != iterStart; iterStart++, positionCounter++) {
        int value = *iterStart;
        int col = positionCounter%gameSize;
        int row = positionCounter/gameSize;

        Piece* piece = new Piece(col,row, value);
        if(piece->getValue() == 0) {
            this->zeroPiece = piece;
        }
        pieces->push_back(piece);
        connect(piece, SIGNAL(clicked(int,int)), this, SLOT(pieceClicked(int,int)));
        scene->addItem(piece);
    }
}

int GameEngine::getMoveCount()
{
    return this->gameSave->getMoves();
}

void GameEngine::saveGame(QString filename)
{
    updateGameSave();
    GameSaveWriter().writeGameSaveTo(this->gameSave, filename);
}

void GameEngine::refreshScene()
{
    if(scene == nullptr) {
        return;
    }

    for(auto iter = pieces->begin(); iter !=pieces->end(); iter++){
        scene->removeItem(*iter);
        scene->addItem(*iter);
    }
}
