#include "startmenu.h"
#include "QLayout"
#include "QMargins"
#include "QLabel"
#include "QFormLayout"
#include <QFileDialog>
#include "gamesaveparser.h"
#include "gamesave.h"

StartMenu::StartMenu(GameWindow* gameWindow, QWidget *parent) :
    QWidget(parent),
    gameWindow(gameWindow)
{
    QFormLayout *inputLayout = new QFormLayout;
    this->gameSizeInput = constructGameSizeInput();
    inputLayout->addRow("Bok układanki: ", gameSizeInput);


    QPushButton *newGameButton = new QPushButton("Nowa gra!");
    QPushButton *loadGameButton = new QPushButton("Wczytaj grę");
    saveGameButton = new QPushButton("Zapisz grę");
    saveGameButton->setEnabled(false);
    QGridLayout *layout = new QGridLayout;

    layout->addLayout(inputLayout, 1, 0);
    layout->addWidget(newGameButton, 2, 0);
    layout->addWidget(saveGameButton, 2, 2);
    layout->addWidget(loadGameButton, 1, 2);

    this->setLayout(layout);

    connect(newGameButton, SIGNAL(clicked(bool)), this, SLOT(openGameWindow(bool)));
    connect(loadGameButton, SIGNAL(clicked(bool)), this, SLOT(loadGame(bool)));
    connect(saveGameButton, SIGNAL(clicked(bool)), gameWindow, SLOT(saveGame(bool)));
}

void StartMenu::sanitizeInput(QString text)
{
    int newValue =0;
    QString::Iterator iter;

    if(text.begin() == text.end()){
        return;
    }

    for(iter = text.begin(); iter< text.end(); iter++){
        if(iter->isDigit()) {
            newValue *= 10;
            newValue += iter->digitValue();
        }
    }
    newValue = newValue > 0 ? newValue : DEFAULT_GAME_SIZE;
    this->gameSizeInput->setText(QString::number(newValue));
}

QLineEdit* StartMenu::constructGameSizeInput() {
    QLineEdit *newInput = new QLineEdit();
    newInput->setMinimumSize(10, 10);
    newInput->setText(QString::number(DEFAULT_GAME_SIZE));
    newInput->setCursorPosition(0);
    newInput->setMaxLength(7);
    connect(newInput, SIGNAL(textEdited(QString)), this, SLOT(sanitizeInput(QString)));
    connect(newInput, SIGNAL(returnPressed()), this, SLOT(openGameWindow()));
    return newInput;
}

void StartMenu::openGameWindow(bool clicked){
    int gameSize = gameSizeInput->text().toInt();
    GameSave* newSave = new GameSave(gameSize);
    gameWindow->openGame(newSave, true);
    gameWindow->show();
    this->saveGameButton->setEnabled(true);
    this->hide();
}

void StartMenu::openGameWindow() {
    openGameWindow(true);
}

void StartMenu::loadGame(bool clicked){
    QString filename = QFileDialog::getOpenFileName(this,
        tr("Wczytaj grę"), "/home/", tr("1337 files(*.leet)"));
    if(!filename.size()){
        return;
    }
    GameSave* gameSave = GameSaveParser().parse(filename);
    gameWindow->openGame(gameSave);
    gameWindow->show();
    this->saveGameButton->setEnabled(true);
    this->hide();
}
