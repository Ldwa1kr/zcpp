#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <QObject>
#include <piece.h>
#include <gamesave.h>
#include <QGraphicsScene>
#include "transformation.h"
/**
 * @brief Klasa zawiadująca mechanizmem gry.
 * Zawiera logikę potrzebną do określenia czy gra się skończyła.
 * Zawiaduje odświeżaniem ekranu
 */
class GameEngine : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Konstruktor
     * @param parent Rodzic
     */
    explicit GameEngine(QObject *parent = nullptr);
    /**
     * @brief Destruktor
     */
    ~GameEngine();
    /**
     * @brief setGameSave
     * @param gameSave
     */
    void setGameSave(GameSave* gameSave);
    /**
     * @brief saveGame
     * @param filename
     */
    void saveGame(QString filename);
    /**
     * @brief randomize
     * @param iterations
     */
    void randomize(int iterations);
    /**
     * @brief getMoveCount
     * @return
     */
    int getMoveCount();
    /**
     * @brief Setter sceny na której będzie malowana gra.
     * @param scene Scena.
     */
    void setScene(QGraphicsScene* scene);

    const QString MANHATTAN_DISTANCE_HEURISTIC = "Odległość Manhattan";
    const QString DISPLACED_HEURISTIC = "Nieuporządkowane klocki";

private:
    /**
     * @brief Wskaźnik na pusty element układanki.
     */
    Piece* zeroPiece;
    /**
     * @brief Scene na której jest malowana gra.
     */
    QGraphicsScene* scene;
    /**
     * @brief Zapis gry.
     */
    GameSave* gameSave;
    /**
     * @brief Wektor elementów układanki znajdujacych się w grze.
     */
    std::vector<Piece*>* pieces;
    /**
    * @brief Flaga czy aktualnie gra się toczy.
    * Flaga czy aktualnie gra się toczy. Zabezpiecza przed uruchomieniem przez użytkownika
    * mechanizmu podpowiadania ruchu poza czasem gry.
     */
    bool gameInProgress;
    /**
     * @brief Flaga czy aktualnie przeprowadzane jest automatyczne rozwiązywanie układanki.
     * Flaga czy aktualnie przeprowadzane jest automatyczne rozwiązywanie układanki. Jeżeli
     * tak, to użytkownik nie powinien móc aktualnie wykonywać samodzielnie ruchów.
     */
    bool currentlySolving = false;
    /**
     * @brief Metoda wykonująca akcje po zakończeniu gry.
     */
    void handleWin();
    /**
     * @brief Metoda zamieniająca lokalizacje elementów układanki ze sobą.
     * @param pieceOne Element Układanki.
     * @param pieceTwo Element Układanki.
     */
    void swapPieceLocations(Piece* pieceOne, Piece* pieceTwo);
    /**
     * @brief Metoda przenosząca element układanki w kierunku oznaczonym przez transformację.
     * @param transformation Transformacja która ma zostać wykonana.
     * @return Czy ruch został wykonany.
     */
    bool movePuzzle(Transformation transformation);
    /**
     * @brief Metoda zwracająca transformację jaka powinna być wykonana po kliknięciu
     * Elementu układanki.
     * @param piece Kliknięty element.
     * @return Transformacja jaka powinna zostac wykonana.
     */
    Transformation getTransformation(Piece* piece);
    /**
     * @brief Metoda zwracająca informację czy gra została zakończona.
     * @return
     */
    bool gameWon();
    /**
     * @brief Metoda aktualizująca zapis gry.
     */
    void updateGameSave();
    /**
     * @brief Metoda odświeżająca scenę na której malowane są elementy układanki.
     */
    void refreshScene();
signals:
    /**
     * @brief Sygnał oznaczający że został wykonany ruch.
     */
    void moveCountChanged();
public slots:
    /**
     * @brief Slot triggerujący mechanizm rozwiązywania gry automatycznie.
     * @param heuristic funkcja heurystyczna do wykorzystania w rozwiązywaniu
     */
    void solve(QString heuristic);
    /**
     * @brief Slot triggerujący mechanizm podpowiadania jednego ruchu.
     * @param heuristic funkcja heurystyczna do wykorzystania w rozwiązywaniu
     */
    void hint(QString heuristic);
    /**
     * @brief Slot rozpoczynający mechanizm poruszania elementem układanki.
     * @param col Kolumna układanki która została kliknięta.
     * @param row Rząd układanki który został kliknięty.
     */
    void pieceClicked(int col, int row);
    /**
     * @brief Slot rozpoczynający mechanizm poruszania elementem układanki.
     * @param transformation Transformacja jaka ma zostać wykonana na układance.
     */
    void userMovePuzzle(Transformation transformation);
};

#endif // GAMEENGINE_H
